const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C"
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C"
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A"

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false
}]

/*

    Q. Get all items that are available (using both Map and Reduce)
    Q. Get all items containing only Vitamin C.
    Q. Get all items containing Vitamin A.
    Q. Group Items based on Vitamins that they contain.
        If an item contains more than 1 vitamin .. 
        Put the copy of the item in each of them.
    Q. Sort items based on number of vitamins they contain.

*/

//PROBLEM 1 USING REDUCE
/* const newArray = items.reduce((acc,curr) => {
    if (curr.available)
    {
        acc.push(curr)
    }
    return acc;
  },[]);
console.log(newArray)
 */

//PROBLEM 1 USING MAP
/* const newArray = items.map((curr) => {
    if (curr.available)
    {
        return curr;
    }
  });
console.log(newArray)
 */

// PROBLEM 2
// let arr = items.filter(function(el){
//     return el.contains === "Vitamin C"
// });
// console.log(arr)

//PROBLEM 3
// let obj = items.filter(item => item.contains.includes("Vitamin A"));
// console.log(obj);

//PROBLEM 4
// let arr =[]
// items.forEach(element => {
//     let vit = element.contains.split(',').map(data => data.trim())
//     vit.map(ele => {
//         if (!arr[ele])
//             arr[ele] = []
//         arr[ele].push(element)
//     })
// })
//console.log(arr)

//PROBLEM 5
 let sortedItems = items.sort((a,b) => {
    if (a.contains.length > b.contains.length)
        return -1;
})

console.log(sortedItems); 
